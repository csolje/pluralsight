student_names = ["James", "Katarina", "Jessica", "Mark", "Bort", "Frank Grimes", "Max Power"]

for name in student_names:
    if name == "Bort":
        continue
        print("Found him! " + name)
    print("Currently testing " + name)

x = 0

while x < 10:
    print("Count is {0}".format(x))
    x += 1
